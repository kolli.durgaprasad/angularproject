// import { Component } from '@angular/core';
// import { ToDoService } from './todo.service';

// @Component({
//   selector: 'app-todos',
//   templateUrl: './todos.component.html',
//   styleUrls: ['./todos.component.css']
// })
// export class TodosComponent implements OnInit {
//   todos: any[];
//   constructor(private todoService: TodoService) { }
//   ngOnInit() {
//     this.todos = this.todoService.getTodos();
//   }
//   addTodo({label}) {
//     this.todos = [{label, id: this.todos.length + 1}, ...this.todos];
//   }
//   completeTodo({todo}) {
//     this.todos = this.todos.map(
//       item => item.id === todo.id ? Object.assign({}, item, {complete: true}) : item
//     );
//   }
//   removeTodo({todo}) {
//     this.todos = this.todos.filter(({id}) => id !== todo.id);
//   }
// }

import { Component, OnInit } from '@angular/core';
import { ToDoService } from './todo.service';
import { ToDo } from '../todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent {
  appTodos: ToDo[];
  constructor(private todoService: ToDoService) {
    this.appTodos = this.todoService.getTodos();
  }

  addTodo(item: { label: string }) {
    this.todoService.addTodo(item);
    this.appTodos = this.todoService.getTodos();
  }
  completeTodo(item: { todo: ToDo }) {
    this.todoService.completeToDo(item.todo);
    this.appTodos = this.todoService.getTodos();
  }


  removeTodo(item: { todo: ToDo }) {
    this.todoService.removeTodo(item.todo);
    this.appTodos = this.todoService.getTodos();
  }
}
