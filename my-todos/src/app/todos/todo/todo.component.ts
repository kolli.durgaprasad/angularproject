import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {
  @Input() item;

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChange = new EventEmitter();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onRemove = new EventEmitter();

  constructor() {}
}

