import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToDo } from '../../todo';


@Component({
  selector: 'app-todolist',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodolistComponent {
  @Input() todos: ToDo[];

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onComplete = new EventEmitter();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onDelete = new EventEmitter();

  constructor() {}
}

