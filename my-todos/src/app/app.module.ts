import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { TodosComponent } from './todos/todos.component';

import { ToDoService } from './todos/todo.service';
import { TodoFormComponent } from './todos/todo-form/todo-form.component';

import { TodoComponent } from './todos/todo/todo.component';
import { TodolistComponent } from './todos/todo-list/todo-list.component';


@NgModule({
  declarations: [AppComponent, TodosComponent, TodoFormComponent, TodoComponent, TodolistComponent],
  imports: [BrowserModule, FormsModule],
  providers: [ ToDoService ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
