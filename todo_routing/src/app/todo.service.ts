import { Injectable } from '@angular/core';
import { ToDo } from './todo';

@Injectable()
export class ToDoService {
  todos: ToDo[] = [
    {
      label: 'Eat pizza',
      id: 0,
      complete: false
    },
    {
      label: 'Do some coding',
      id: 1,
      complete: false
    },
    {
      label: 'Sleep',
      id: 2,
      complete: false
    },
    {
      label: 'Print tickets',
      id: 3,
      complete: false
    }
  ];

  getTodos() {
    return this.todos;
  }

  addTodo(item: { label: string }) {
    this.todos = [
      {
        label: item.label,
        id: this.todos.length + 1,
        complete: false
      },
      ...this.todos
    ];
  }

  completeToDo(todo: ToDo) {
    this.todos = this.todos.map(
      item => (item.id === todo.id ? { ...item, complete: true } : item)
    );
  }

  removeTodo(todo: ToDo) {
    this.todos = this.todos.filter(it => it.id !== todo.id);
  }

  findById(id: number) {
    return this.todos.find(it => it.id === id);
  }

  update(todo) {
    this.todos = [...this.todos.filter(it => it.id !== todo.id), todo];
  }
}
