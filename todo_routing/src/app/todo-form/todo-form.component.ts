import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Todo } from '../todouser';
import { ToDo } from '../todo';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent {
  label: string;
  // todos: Todo = { label: '' };
  // onSubmit({ value }: { value: Todo }) {
  //   console.log(value);
  // }
//  tslint:disable-next-line:no-output-on-prefix
  @Output() onAdd = new EventEmitter();

  submit() {
    // tslint:disable-next-line:curly
    if (!this.label) return;
    this.onAdd.emit({ label: this.label });
    this.label = '';
  }
}
