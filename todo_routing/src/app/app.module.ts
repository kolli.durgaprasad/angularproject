import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ToDoService } from './todo.service';
import { TodoComponent } from './todo/todo.component';
import { TodolistComponent } from './todolist/todolist.component';
import { TodosComponent } from './todos/todos.component';
import { TodoFormComponent } from './todo-form/todo-form.component';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
// import { TodoFormSmartComponent } from './todo-form/todo-form-smart.component';

import { Routes, RouterModule } from '@angular/router';
import { TodoFormSmartComponent } from './todo-form-smart/todo-form-smart.component';
import { TodoEditComponent } from './todo-edit/todo-edit.component';
import { TodoEditSmartComponent } from './todo-edit-smart/todo-edit-smart.component';


export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'about', component: AboutComponent },
  { path: 'todos', component: TodosComponent },
  { path: 'todo/new', component: TodoFormSmartComponent },
  { path: 'todos/:id/edit', component: TodoEditSmartComponent }];


@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodolistComponent,
    TodosComponent,
    TodoFormComponent,
    TodoFormSmartComponent,
    TodoEditComponent,
    TodoEditSmartComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent
    // TodoFormSmartComponent
  ],
  imports: [BrowserModule, FormsModule, RouterModule.forRoot(ROUTES)],
  providers: [ToDoService],
  bootstrap: [AppComponent]
})
export class AppModule {}
